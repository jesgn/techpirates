### Programming
- [FunctionalCs](https://functionalcs.github.io/curriculum/)
- [Learnxinyminutes](https://learnxinyminutes.com/)
- [Codewars](https://www.codewars.com/)
- [CodeBerg](https://codeberg.org/)
- [Git Intro](https://rogerdudler.github.io/git-guide/)

### Privacy & Security
- [AwesomePrivacy](https://github.com/pluja/awesome-privacy)
- [PrivacyTools](https://www.privacytools.io/)
- [PrismBreak](https://prism-break.org/en/)
- [The Hitchhiker’s Guide to Online Anonymity](https://anonymousplanet.org/guide.html)
- [I2p](https://geti2p.net/en/)
- [Tor](https://www.torproject.org/)
- [Mullvad](https://mullvad.net/en)
- [Njalla](https://njal.la/)
- [Vx-underground](https://www.vx-underground.org/)
- [TurtleCute](https://turtlecute.org)

### Wargames & Ctf
- [OverTheWire](https://overthewire.org/wargames/)
- [Hackthebox](https://www.hackthebox.com/)
- [Hackthissite](https://www.hackthissite.org)

### News
- [HackerNews](https://news.ycombinator.com/)
- [TheHackerNews](https://thehackernews.com/)

### Operating Systems
- [GrapheneOs](https://grapheneos.org/)
- [Whonix](https://www.whonix.org/)
- [Qubeos](https://www.qubes-os.org/)
- [Tails](https://tails.boum.org/)
- [Gentoo](https://www.gentoo.org/)

### CryptoCurrency
- [KycNotMe](https://kycnot.me/)
- [Rekt](https://rekt.news/)
- [CryptoPanic](https://cryptopanic.com/)

### Blogs
- [sizeof](https://sizeof.cat/)
- [gwern](https://www.gwern.net/)
- [ed](https://eldritchdata.neocities.org/)
- [Archiveteam](https://wiki.archiveteam.org/index.php/Main_Page)
- [SethForPrivacy](https://sethforprivacy.com)

### Random Links
- [MotherFuckingWebsite](https://motherfuckingwebsite.com/)
- [nakamotoinstitute](https://nakamotoinstitute.org/literature/)
